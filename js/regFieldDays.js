//submit form
jQuery("#Submit").on("click", function(e){

    e.preventDefault();

    // Grab inputs and store as variables
    var FirstName = jQuery("#FirstName").val(),
    LastName = jQuery("#LastName").val(),
    Email = jQuery("#Email").val(),
    ConfEmail = jQuery("#ConfEmail").val(),
    Organization = jQuery("#Organization").val(),
    Consent = jQuery("#Consent").is(":checked"),
    Timestamp = new Date().toString(),
    JobTitle = jQuery("#JobTitle").val(),
    Country = jQuery("#Country").children(":selected").val(),
    ZipCode = jQuery("#ZipCode").val(),
    DescribeOrganization = jQuery("#DescribeOrganization").children(":selected").val(),
    Morning = jQuery("#Morning").is(":checked"),
    Afternoon = jQuery("#Afternoon").is(":checked"),
    Referral = jQuery("#ReferralRadio").find("input[type=radio]:checked").val(),
    missingField = false,
    badEmail = false,
    mismatch = false,
    errorArr = [];

    function removeError(e){
        e.currentTarget.parentNode.classList.remove("error");
    }

    // Validation for email and text inputs below

    jQuery("#sat_form .sat_form-input:not(#ConfEmail)").each(function(){
        if(jQuery(this).val() == "" || jQuery(this).val() == null || jQuery(this).val() == undefined){
            jQuery(this).parent().addClass("error");
            jQuery(this).on("keyup change", removeError);
            missingField = true;
        } else {
          jQuery(this).parent().removeClass("error");
        }
    });

    function testEmail(email){
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    if (!testEmail(Email)){
        jQuery("#Email").parent().addClass("error");
        jQuery("#Email").on("keyup change", removeError);
        badEmail = true;
    } else {
      jQuery("#Email").parent().removeClass("error");
    }

    if(ConfEmail !== Email){
      jQuery("#ConfEmail").parent().addClass("error");
      jQuery("#ConfEmail").on("keyup change", removeError);
      mismatch = true;
    } else {
      jQuery("#ConfEmail").parent().removeClass("error");
    }

    jQuery(".sat_form-select").each(function(){
      if(jQuery(this).children(":selected").val() == "none"){
        jQuery(this).parent().addClass("error");
        jQuery(this).on("change", removeError);
        missingField = true;
      } else {
        jQuery(this).parent().removeClass("error");
      }
    });

    if(!jQuery("#sat_form input[type=checkbox]").is(":checked")){
      jQuery("#EventsChecks").parent().addClass("error");
      jQuery("#EventsChecks").on("change", removeError);
      missingField = true;
    } else {
      jQuery("#EventsChecks").parent().removeClass("error");
    }

    if(!jQuery("#sat_form input[type=radio]").is(":checked")){
      jQuery("#ReferralRadio").parent().addClass("error");
      jQuery("#ReferralRadio").on("change", removeError);
      missingField = true;
    } else {
      jQuery("#ReferralRadio").parent().removeClass("error");
    }

    if(missingField){
      errorArr.push("Please fill out required fields.")
    }

    if(badEmail){
      errorArr.push("Please provide a valid email address.")
    }

    if(mismatch){
      errorArr.push("Emails do not match.")
    }

    jQuery("#error-list").text(errorArr.join(" "));

    // Submit function
    if(jQuery("#sat_form").find(".error").length === 0){
        jQuery.ajax({
            type: "POST",
            url: 'https://pub.s4.exacttarget.com/cbzqo0ca200',
            data: {
                FirstName: FirstName,
                LastName: LastName,
                Email: Email,
                Timestamp: Timestamp,
                Organization: Organization,
                Consent: Consent,
                JobTitle: JobTitle,
                Country: Country,
                ZipCode: ZipCode,
                DescribeOrganization: DescribeOrganization,
                Morning: Morning,
                Afternoon: Afternoon,
                Referral: Referral
            },
            crossDomain: true,
            success : function (data, response, statusText) {
                if(data.indexOf("Duplicate") > -1){
                  // Duplicate email. User has already registered with this email.
                    jQuery("#sat_form").addClass("hidden");
                    jQuery("#alreadyRegistered").addClass("d-block");
                } else if (data.indexOf("success") > -1){
                  // Success! Form went through. Do post success stuff here.
                    jQuery("#sat_form").addClass("hidden");
                    jQuery("#submitSuccess").addClass("d-block");
                };
            },
            error: function(data, response, statusText){
              // Data didn't make it to MC. Contact devs at G&S if persists.
                alert("Something went wrong. Please try again later.");
            }
        });
    } else {
        return;
    };
});
//End Form Submission Code

//Countdown Timer
dateFuture = new Date(2020,8,10,8,0,0);

function GetCount(){

        dateNow = new Date();                                                                        //grab current date
        amount = dateFuture.getTime() - dateNow.getTime();                //calc milliseconds between dates
        delete dateNow;

        // time is already past
        if(amount < 0){
                document.getElementById('countbox').innerHTML="Now!";
        }
        // date is still good
        else{
                days=0;hours=0;mins=0;secs=0;out="";

                amount = Math.floor(amount/1000);//kill the "milliseconds" so just secs

                days=Math.floor(amount/86400);//days
                amount=amount%86400;

                hours=Math.floor(amount/3600);//hours
                amount=amount%3600;

                mins=Math.floor(amount/60);//minutes
                amount=amount%60;

                secs=Math.floor(amount);//seconds

                if(days != 0){out += days +"D"+" : ";}
                if(days != 0 || hours != 0){out += hours +"H"+" : ";}
                if(days != 0 || hours != 0 || mins != 0){out += mins +"M"+" : ";}
                out += secs +"S";
                document.getElementById('countbox').innerHTML=out;

                setTimeout("GetCount()", 1000);
        }
}

window.onload=function(){GetCount();}//call when everything has loaded


//Smooth scroll to on-page targets
// Select all links with hashes
jQuery(document).ready(function () {
  jQuery('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '')
        &&
        location.hostname === this.hostname
        ) {
        // Figure out element to scroll to
        var target = jQuery(this.hash);
        target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        jQuery('html, body').animate({
          scrollTop: target.offset().top - (40) //ets the scroll to alightly above the target
          }, 1000, function () {
          //Setting focus causes a jump from the offset target. We simply unset the focus instead
          document.activeElement.blur();
        });
      }
    }
  }); //end smooth scroll
});
