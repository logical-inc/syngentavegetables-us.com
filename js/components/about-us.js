Vue.component('about-us', { template: `
<div class="container-fluid pt-4 pb-5 px-4 px-sm-5" id="about">
  <div class="row pb-4">
    <div class="col-12 col-xl-6 text-center px-xl-5 pb-5 pb-xl-0">
      <h2 class="pb-4 text-brand">Locally owned and operated since 1989</h2>
      <p class="mb-0">Elk Grove Lock & Safe began operations in 1989. We are one of Sacramento County’s oldest locksmith companies, and the only locksmith based in Elk Grove; we are completely locally owned and operated. We offer both a state of the art locksmith shop and a fast, professional mobile service. Our technicians are some of the most experienced in the area, and most have been a part of Elk Grove Lock &amp; Safe for more than 10 years. We consider our technicians our most valuable asset. Because of them, we have the products and knowledge to handle all of your locksmith needs.</p>
    </div>
    <div class="col-12 col-xl-6 text-center px-xl-5">
      <h2 class="pb-4 text-brand">Our Guarantee to You</h2>
      <p class="mb-0">
        It's not just luck that we've been in business for over thirty years! We pride ourselves on our service and customer satisfaction, knowing that when our clients call, they're probably going to need us there yesterday. On top of our professional and prompt service, we are fully insured and licensed by consumer affairs as a California Certified Small Business. Did we mention we're family owned too? Call us today and ensure your locks and belongings and protected by Elk Grove's only locally operated Locksmith.
        <small class="d-block"><em>Contractor Lic: #776935 • Lic. #LC0 2018 Insured PL/PD • CA Certified Small Business #0038725</em></small>
      </p>
    </div>
  </div>
</div>
`})
