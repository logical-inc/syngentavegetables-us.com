Vue.component('registration-form', { template: `
<div class="form-wrapper">
  <div id="sat_form">
    <div class="sat_form-intro col-12 pb-3">
      <h2 class="text-brand-alt-darker">Tune in September 10, 2020</h2>
      <p class="legal disclaimer">All fields required. We will use your information only in accordance with our <a href="https://www.syngenta-us.com/legal/privacypolicy.html" target="_blank">privacy policy</a>.</p>
    </div>
    <div class="sat_form-group col-12 col-sm-6">
      <label for="FirstName" class="sat_form-label">First Name</label>
      <input type="text" name="FirstName" id="FirstName" class="sat_form-input" />
    </div>
    <div class="sat_form-group col-12 col-sm-6">
      <label for="LastName" class="sat_form-label">Last Name</label>
      <input type="text" name="LastName" id="LastName" class="sat_form-input" />
    </div>
    <div class="sat_form-group col-12 col-sm-6">
      <label for="Email" class="sat_form-label">Email Address</label>
      <input type="email" name="Email" id="Email" class="sat_form-input" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" />
    </div>
    <div class="sat_form-group col-12 col-sm-6">
      <label for="ConfEmail" class="sat_form-label">Confirm Email</label>
      <input type="email" name="ConfEmail" id="ConfEmail" class="sat_form-input" />
    </div>
    <div class="sat_form-group col-12 col-sm-6">
      <label for="Organization" class="sat_form-label">Organization</label>
      <input type="text" name="Organization" id="Organization" class="sat_form-input" />
    </div>
    <div class="sat_form-group col-12 col-sm-6">
      <label for="JobTitle" class="sat_form-label">Job Title</label>
      <input type="text" name="JobTitle" id="JobTitle" class="sat_form-input" />
    </div>
    <div class="sat_form-group col-12 col-sm-6">
      <label for="Country">Country</label>
      <select id="Country" class="sat_form-select" name="Country">
        <option selected disabled value="none">Select</option>
          <option class="sat_form-select-item" value="Afghanistan">Afghanistan </option>
          <option class="sat_form-select-item" value="Albania">Albania </option>
          <option class="sat_form-select-item" value="Algeria">Algeria </option>
          <option class="sat_form-select-item" value="Andorra">Andorra </option>
          <option class="sat_form-select-item" value="Angola">Angola </option>
          <option class="sat_form-select-item" value="Antigua and Barbuda">Antigua and Barbuda </option>
          <option class="sat_form-select-item" value="Argentina">Argentina </option>
          <option class="sat_form-select-item" value="Armenia">Armenia </option>
          <option class="sat_form-select-item" value="Australia">Australia </option>
          <option class="sat_form-select-item" value="Austria">Austria </option>
          <option class="sat_form-select-item" value="Azerbaijan">Azerbaijan </option>
          <option class="sat_form-select-item" value="The Bahamas">The Bahamas </option>
          <option class="sat_form-select-item" value="Bahrain">Bahrain </option>
          <option class="sat_form-select-item" value="Bangladesh">Bangladesh </option>
          <option class="sat_form-select-item" value="Barbados">Barbados </option>
          <option class="sat_form-select-item" value="Belarus">Belarus </option>
          <option class="sat_form-select-item" value="Belgium">Belgium </option>
          <option class="sat_form-select-item" value="Belize">Belize </option>
          <option class="sat_form-select-item" value="Benin">Benin </option>
          <option class="sat_form-select-item" value="Bhutan">Bhutan </option>
          <option class="sat_form-select-item" value="Bolivia">Bolivia </option>
          <option class="sat_form-select-item" value="Bosnia and Herzegovina">Bosnia and Herzegovina </option>
          <option class="sat_form-select-item" value="Botswana">Botswana </option>
          <option class="sat_form-select-item" value="Brazil">Brazil </option>
          <option class="sat_form-select-item" value="Brunei">Brunei </option>
          <option class="sat_form-select-item" value="Bulgaria">Bulgaria </option>
          <option class="sat_form-select-item" value="Burkina Faso">Burkina Faso </option>
          <option class="sat_form-select-item" value="Burundi">Burundi </option>
          <option class="sat_form-select-item" value="Cabo Verde">Cabo Verde </option>
          <option class="sat_form-select-item" value="Cambodia">Cambodia </option>
          <option class="sat_form-select-item" value="Cameroon">Cameroon </option>
          <option class="sat_form-select-item" value="Canada">Canada </option>
          <option class="sat_form-select-item" value="Central African Republic">Central African Republic </option>
          <option class="sat_form-select-item" value="Chad">Chad </option>
          <option class="sat_form-select-item" value="Chile">Chile </option>
          <option class="sat_form-select-item" value="China">China </option>
          <option class="sat_form-select-item" value="Colombia">Colombia </option>
          <option class="sat_form-select-item" value="Comoros">Comoros </option>
          <option class="sat_form-select-item" value="Congo, Democratic Republic of the">Congo, Democratic Republic of the </option>
          <option class="sat_form-select-item" value="Congo, Republic of the">Congo, Republic of the </option>
          <option class="sat_form-select-item" value="Costa Rica">Costa Rica </option>
          <option class="sat_form-select-item" value="Côte d’Ivoire">Côte d’Ivoire </option>
          <option class="sat_form-select-item" value="Croatia">Croatia </option>
          <option class="sat_form-select-item" value="Cuba">Cuba </option>
          <option class="sat_form-select-item" value="Cyprus">Cyprus </option>
          <option class="sat_form-select-item" value="Czech Republic">Czech Republic </option>
          <option class="sat_form-select-item" value="Denmark">Denmark </option>
          <option class="sat_form-select-item" value="Djibouti">Djibouti </option>
          <option class="sat_form-select-item" value="Dominica">Dominica </option>
          <option class="sat_form-select-item" value="Dominican Republic">Dominican Republic </option>
          <option class="sat_form-select-item" value="East Timor (Timor-Leste)">East Timor (Timor-Leste) </option>
          <option class="sat_form-select-item" value="Ecuador">Ecuador </option>
          <option class="sat_form-select-item" value="Egypt">Egypt </option>
          <option class="sat_form-select-item" value="El Salvador">El Salvador </option>
          <option class="sat_form-select-item" value="Equatorial Guinea">Equatorial Guinea </option>
          <option class="sat_form-select-item" value="Eritrea">Eritrea </option>
          <option class="sat_form-select-item" value="Estonia">Estonia </option>
          <option class="sat_form-select-item" value="Eswatini">Eswatini </option>
          <option class="sat_form-select-item" value="Ethiopia">Ethiopia </option>
          <option class="sat_form-select-item" value="Fiji">Fiji </option>
          <option class="sat_form-select-item" value="Finland">Finland </option>
          <option class="sat_form-select-item" value="France">France </option>
          <option class="sat_form-select-item" value="Gabon">Gabon </option>
          <option class="sat_form-select-item" value="The Gambia">The Gambia </option>
          <option class="sat_form-select-item" value="Georgia">Georgia </option>
          <option class="sat_form-select-item" value="Germany">Germany </option>
          <option class="sat_form-select-item" value="Ghana">Ghana </option>
          <option class="sat_form-select-item" value="Greece">Greece </option>
          <option class="sat_form-select-item" value="Grenada">Grenada </option>
          <option class="sat_form-select-item" value="Guatemala">Guatemala </option>
          <option class="sat_form-select-item" value="Guinea">Guinea </option>
          <option class="sat_form-select-item" value="Guinea-Bissau">Guinea-Bissau </option>
          <option class="sat_form-select-item" value="Guyana">Guyana </option>
          <option class="sat_form-select-item" value="Haiti">Haiti </option>
          <option class="sat_form-select-item" value="Honduras">Honduras </option>
          <option class="sat_form-select-item" value="Hungary">Hungary </option>
          <option class="sat_form-select-item" value="Iceland">Iceland </option>
          <option class="sat_form-select-item" value="India">India </option>
          <option class="sat_form-select-item" value="Indonesia">Indonesia </option>
          <option class="sat_form-select-item" value="Iran">Iran </option>
          <option class="sat_form-select-item" value="Iraq">Iraq </option>
          <option class="sat_form-select-item" value="Ireland">Ireland </option>
          <option class="sat_form-select-item" value="Israel">Israel </option>
          <option class="sat_form-select-item" value="Italy">Italy </option>
          <option class="sat_form-select-item" value="Jamaica">Jamaica </option>
          <option class="sat_form-select-item" value="Japan">Japan </option>
          <option class="sat_form-select-item" value="Jordan">Jordan </option>
          <option class="sat_form-select-item" value="Kazakhstan">Kazakhstan </option>
          <option class="sat_form-select-item" value="Kenya">Kenya </option>
          <option class="sat_form-select-item" value="Kiribati">Kiribati </option>
          <option class="sat_form-select-item" value="Korea, North">Korea, North </option>
          <option class="sat_form-select-item" value="Korea, South">Korea, South </option>
          <option class="sat_form-select-item" value="Kosovo">Kosovo </option>
          <option class="sat_form-select-item" value="Kuwait">Kuwait </option>
          <option class="sat_form-select-item" value="Kyrgyzstan">Kyrgyzstan </option>
          <option class="sat_form-select-item" value="Laos">Laos </option>
          <option class="sat_form-select-item" value="Latvia">Latvia </option>
          <option class="sat_form-select-item" value="Lebanon">Lebanon </option>
          <option class="sat_form-select-item" value="Lesotho">Lesotho </option>
          <option class="sat_form-select-item" value="Liberia">Liberia </option>
          <option class="sat_form-select-item" value="Libya">Libya </option>
          <option class="sat_form-select-item" value="Liechtenstein">Liechtenstein </option>
          <option class="sat_form-select-item" value="Lithuania">Lithuania </option>
          <option class="sat_form-select-item" value="Luxembourg">Luxembourg </option>
          <option class="sat_form-select-item" value="Madagascar">Madagascar </option>
          <option class="sat_form-select-item" value="Malawi">Malawi </option>
          <option class="sat_form-select-item" value="Malaysia">Malaysia </option>
          <option class="sat_form-select-item" value="Maldives">Maldives </option>
          <option class="sat_form-select-item" value="Mali">Mali </option>
          <option class="sat_form-select-item" value="Malta">Malta </option>
          <option class="sat_form-select-item" value="Marshall Islands">Marshall Islands </option>
          <option class="sat_form-select-item" value="Mauritania">Mauritania </option>
          <option class="sat_form-select-item" value="Mauritius">Mauritius </option>
          <option class="sat_form-select-item" value="Mexico – Eastern region">Mexico – Eastern region </option>
          <option class="sat_form-select-item" value="Mexico – Western region">Mexico – Western region </option>
          <option class="sat_form-select-item" value="Micronesia, Federated States of">Micronesia, Federated States of </option>
          <option class="sat_form-select-item" value="Moldova">Moldova </option>
          <option class="sat_form-select-item" value="Monaco">Monaco </option>
          <option class="sat_form-select-item" value="Mongolia">Mongolia </option>
          <option class="sat_form-select-item" value="Montenegro">Montenegro </option>
          <option class="sat_form-select-item" value="Morocco">Morocco </option>
          <option class="sat_form-select-item" value="Mozambique">Mozambique </option>
          <option class="sat_form-select-item" value="Myanmar (Burma)">Myanmar (Burma) </option>
          <option class="sat_form-select-item" value="Namibia">Namibia </option>
          <option class="sat_form-select-item" value="Nauru">Nauru </option>
          <option class="sat_form-select-item" value="Nepal">Nepal </option>
          <option class="sat_form-select-item" value="Netherlands">Netherlands </option>
          <option class="sat_form-select-item" value="New Zealand">New Zealand </option>
          <option class="sat_form-select-item" value="Nicaragua">Nicaragua </option>
          <option class="sat_form-select-item" value="Niger">Niger </option>
          <option class="sat_form-select-item" value="Nigeria">Nigeria </option>
          <option class="sat_form-select-item" value="North Macedonia">North Macedonia </option>
          <option class="sat_form-select-item" value="Norway">Norway </option>
          <option class="sat_form-select-item" value="Oman">Oman </option>
          <option class="sat_form-select-item" value="Pakistan">Pakistan </option>
          <option class="sat_form-select-item" value="Palau">Palau </option>
          <option class="sat_form-select-item" value="Panama">Panama </option>
          <option class="sat_form-select-item" value="Papua New Guinea">Papua New Guinea </option>
          <option class="sat_form-select-item" value="Paraguay">Paraguay </option>
          <option class="sat_form-select-item" value="Peru">Peru </option>
          <option class="sat_form-select-item" value="Philippines">Philippines </option>
          <option class="sat_form-select-item" value="Poland">Poland </option>
          <option class="sat_form-select-item" value="Portugal">Portugal </option>
          <option class="sat_form-select-item" value="Qatar">Qatar </option>
          <option class="sat_form-select-item" value="Romania">Romania </option>
          <option class="sat_form-select-item" value="Russia">Russia </option>
          <option class="sat_form-select-item" value="Rwanda">Rwanda </option>
          <option class="sat_form-select-item" value="Saint Kitts and Nevis">Saint Kitts and Nevis </option>
          <option class="sat_form-select-item" value="Saint Lucia">Saint Lucia </option>
          <option class="sat_form-select-item" value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines </option>
          <option class="sat_form-select-item" value="Samoa">Samoa </option>
          <option class="sat_form-select-item" value="San Marino">San Marino </option>
          <option class="sat_form-select-item" value="Sao Tome and Principe">Sao Tome and Principe </option>
          <option class="sat_form-select-item" value="Saudi Arabia">Saudi Arabia </option>
          <option class="sat_form-select-item" value="Senegal">Senegal </option>
          <option class="sat_form-select-item" value="Serbia">Serbia </option>
          <option class="sat_form-select-item" value="Seychelles">Seychelles </option>
          <option class="sat_form-select-item" value="Sierra Leone">Sierra Leone </option>
          <option class="sat_form-select-item" value="Singapore">Singapore </option>
          <option class="sat_form-select-item" value="Slovakia">Slovakia </option>
          <option class="sat_form-select-item" value="Slovenia">Slovenia </option>
          <option class="sat_form-select-item" value="Solomon Islands">Solomon Islands </option>
          <option class="sat_form-select-item" value="Somalia">Somalia </option>
          <option class="sat_form-select-item" value="South Africa">South Africa </option>
          <option class="sat_form-select-item" value="Spain">Spain </option>
          <option class="sat_form-select-item" value="Sri Lanka">Sri Lanka </option>
          <option class="sat_form-select-item" value="Sudan">Sudan </option>
          <option class="sat_form-select-item" value="Sudan, South">Sudan, South </option>
          <option class="sat_form-select-item" value="Suriname">Suriname </option>
          <option class="sat_form-select-item" value="Sweden">Sweden </option>
          <option class="sat_form-select-item" value="Switzerland">Switzerland </option>
          <option class="sat_form-select-item" value="Syria">Syria </option>
          <option class="sat_form-select-item" value="Taiwan">Taiwan </option>
          <option class="sat_form-select-item" value="Tajikistan">Tajikistan </option>
          <option class="sat_form-select-item" value="Tanzania">Tanzania </option>
          <option class="sat_form-select-item" value="Thailand">Thailand </option>
          <option class="sat_form-select-item" value="Togo">Togo </option>
          <option class="sat_form-select-item" value="Tonga">Tonga </option>
          <option class="sat_form-select-item" value="Trinidad and Tobago">Trinidad and Tobago </option>
          <option class="sat_form-select-item" value="Tunisia">Tunisia </option>
          <option class="sat_form-select-item" value="Turkey">Turkey </option>
          <option class="sat_form-select-item" value="Turkmenistan">Turkmenistan </option>
          <option class="sat_form-select-item" value="Tuvalu">Tuvalu </option>
          <option class="sat_form-select-item" value="Uganda">Uganda </option>
          <option class="sat_form-select-item" value="Ukraine">Ukraine </option>
          <option class="sat_form-select-item" value="United Arab Emirates">United Arab Emirates </option>
          <option class="sat_form-select-item" value="United Kingdom">United Kingdom </option>
          <option class="sat_form-select-item" value="United States – Western region">United States – Western region </option>
          <option class="sat_form-select-item" value="United States – Northeast region">United States – Northeast region </option>
          <option class="sat_form-select-item" value="United States – Southeast region">United States – Southeast region </option>
          <option class="sat_form-select-item" value="United States – Other region">United States – Other region </option>
          <option class="sat_form-select-item" value="Uruguay">Uruguay </option>
          <option class="sat_form-select-item" value="Uzbekistan">Uzbekistan </option>
          <option class="sat_form-select-item" value="Vanuatu">Vanuatu </option>
          <option class="sat_form-select-item" value="Vatican City">Vatican City </option>
          <option class="sat_form-select-item" value="Venezuela">Venezuela </option>
          <option class="sat_form-select-item" value="Vietnam">Vietnam </option>
          <option class="sat_form-select-item" value="Yemen">Yemen </option>
          <option class="sat_form-select-item" value="Zambia">Zambia </option>
          <option class="sat_form-select-item" value="Zimbabwe">Zimbabwe </option>
      </select>
    </div>
    <div class="sat_form-group col-12 col-sm-6">
      <label for="ZipCode" class="sat_form-label">Zip/Postal Code</label>
      <input type="text" name="ZipCode" id="ZipCode" class="sat_form-input" />
    </div>
    <div class="sat_form-group col-12">
      <label for="DescribeOrganization" class="sat_form-label">What best describes your business/organization?</label>
      <select class="sat_form-select" id="DescribeOrganization" name="DescribeOrganization">
        <option selected disabled value="none">Select</option>
        <option class="sat_form-select-item" value="Grower-Shipper">Grower-Shipper</option>
        <option class="sat_form-select-item" value="Distributor">Distributor</option>
        <option class="sat_form-select-item" value="Supermarket/Retail">Supermarket/Retail</option>
        <option class="sat_form-select-item" value="Food Service">Food Service</option>
        <option class="sat_form-select-item" value="Technology or Business Service Provider">Technology or Business Service Provider</option>
        <option class="sat_form-select-item" value="Supplier">Supplier</option>
        <option class="sat_form-select-item" value="Wholesales">Wholesales</option>
        <option class="sat_form-select-item" value="Media">Media</option>
        <option class="sat_form-select-item" value="Promotional Organization/Association">Promotional Organization/Association</option>
      </select>
    </div>
    <div class="sat_form-group col-12">
      <fieldset id="EventsChecks" class="sat_form-fieldset">
          <legend class="sat_form-legend">Which events do you plan to attend?</legend>
          <span class="sat_form-fieldset-group">
            <input type="checkbox" name="Morning" id="Morning" value="Morning">
            <label for="Morning">Morning session<br>(South America, Central America & the Caribbean)</label>
        </span>
          <span class="sat_form-fieldset-group">
            <input type="checkbox" name="Afternoon" id="Afternoon" value="Afternoon">
            <label for="Afternoon">Afternoon session<br>(Canada, Mexico & U.S.A.)</label>
        </span>
      </fieldset>
    </div>
    <div class="sat_form-group col-12">
      <fieldset id="ReferralRadio" class="sat_form-fieldset">
        <legend class="sat_form-legend">How did you hear about this event?</legend>
        <div class="row">
          <span class="sat_form-fieldset-group col-7 col-sm-6">
            <input type="radio" name="referral" id="SyngentaRep" value="Syngenta rep">
            <label for="SyngentaRep">Syngenta rep</label>
          </span>
          <span class="sat_form-fieldset-group col-5 col-sm-6">
            <input type="radio" name="referral" id="EmailRadio" value="Email">
            <label for="EmailRadio">Email</label>
          </span>
          <span class="sat_form-fieldset-group col-7 col-sm-6">
            <input type="radio" name="referral" id="SocialMedia" value="Social media">
            <label for="SocialMedia">Social media</label>
          </span>
          <span class="sat_form-fieldset-group col-5 col-sm-6">
            <input type="radio" name="referral" id="OtherRadio" value="Other">
            <label for="OtherRadio">Other</label>
          </span>
        </div>
      </fieldset>
    </div>
    <div class="sat_form-group col-12">
      <span class="sat_form-fieldset-group">
        <input type="checkbox" id="Consent" name="Consent" value="Agree" />
        <label for="Consent"><strong>Check to receive future Syngenta Vegetables news</strong></label>
      </span>
    </div>
    <div class="sat_form-group col-12 pt-2 pt-lg-3">
      <input type="submit" value="Register" class="btn btn-submit w-75 mx-auto" id="Submit"/>
    </div>
    <div class="sat_form-group col-12">
      <p id="error-list"></p>
    </div>
  </div>
  <div class="py-5 text-center d-none" id="submitSuccess">
    <h3>Thank you for registering for Sygenta Americas Tour Live! on September 10th.</h3>
    <p class="pt-3">You will receive your registration confirmation and event reminders in your inbox. We look forward to seeing you soon!</p>
    <img src="https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/08/10/logo.png" class="img-fluid mt-4" style="width:260px;">
  </div>
  <div class="py-5 text-center d-none" id="alreadyRegistered">
    <h3>You have already registered for the event using this email.</h3>
    <p class="pt-3">Please check your inbox and spam folder again for the registration confirmation email.</p>
    <img src="https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/08/10/logo.png" class="img-fluid mt-4" style="width:260px;">
  </div>
</div>
`})
