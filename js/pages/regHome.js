const PageHome = { template: `
  <div class="page-home">
    <div class="container-fluid banner banner-home">
      <div class="row">
        <div class="container">
          <div class="row justify-content-between position-relative">
            <div class="banner-content">
              <div class="banner-content-item">
                <h1 class="pb-4">Syngenta Americas Tour - Live!</h1>
                <p class="mb-0">
                  No travel, no problem. To ensure growers, seed distributors and partners along the value chain are still equipped with the latest products and innovations despite pandemic-related restrictions, Syngenta Vegetable Seeds is creating its first live, virtual field day event.
                  <b>Check back here on September 10 starting at 8AM PST to stream live!</b>
                </p>
                <div id="countbox" class="countbox mt-3 text-center"></div>
              </div>
            </div>
            <div class="banner-registration registration" id="registration-form">
              <div class="banner-registration-content">
                <registration-form></registration-form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container py-4">
      <div class="row py-4 justify-content-center align-items-stretch">
        <div class="col-12">
          <h2 class="pb-5 text-center">
            Syngenta brings field days to screens across the Americas with live, virtual cucurbits event.
          </h2>
        </div>
        <div class="col-12 col-md-5 mb-5 mb-md-0 d-flex align-items-stretch">
          <div class="registration-switch-image"></div>
        </div>
        <div class="col-12 pb-3 pb-md-0 col-md-7 pl-xl-5">
          <p>
            At Syngenta Vegetable Seeds, growers are at the heart of everything we do. We love being in
            the field with you, to share, to learn and to serve. 2020 has bought some new challenges to the
            seed industry, and we put safety first for our growers, partners and all individuals who work in
            our industry across the value chain. So, this year, stay safe and stay home and let Syngenta
            come to you virtually. To share our new products and innovations, please join us September 10,
            2020 for <b>Syngenta Americas Tour – Live! A Virtual Cucurbits Experience</b>.
          </p>
          <p>
            The live, virtual
            event will highlight the latest commercial and experimental cucurbit innovations in the Syngenta
            portfolio. During the event, which will be remotely broadcast from our research station in
            Woodland, attendees will have the opportunity to tune in for multiple sessions, then interact live
            with our breeding and sales team. Each session of the event will have its own regional focus,
            covering topics and varieties of key interest for those demographics, as well as speakers in
            multiple languages.
          </p>
          <div class="row">
            <div class="col-12 pt-4 mt-2 add-to-calendar">

              <div class="add-to-calendar-item">
                <!--
                <div title="Add to Calendar" class="addeventatc item-button">
                  Add to Calendar (Session 1)
                  <span class="start">09/10/2020 08:00 AM</span>
                  <span class="end">09/10/2020 11:00 AM</span>
                  <span class="timezone">America/Los_Angeles</span>
                  <span class="title">Sygenta Virtual Field Days (Session 1)</span>
                  <span class="description">Interact live with Syngenta's breeding and sales teams, focussing on new products for South America, Central America and the Caribbean</span>
                  <span class="location">https://www.syngentavegetables-us.com/</span>
                </div>
                -->
                <div class="item-icon"><i class="fa fa-calendar-o" aria-hidden="true"></i></div>
                <div class="item-content">
                  <span class="item-title">Morning Session: 8 AM – 11 AM Pacific Time</span><br>
                  <em>Focus: South America, Central America and the Caribbean</em><br>
                </div>
              </div>

              <div class="add-to-calendar-item">
                <!--
                <div title="Add to Calendar" class="addeventatc item-button">
                  Add to Calendar (Session 2)
                  <span class="start">09/10/2020 01:00 PM</span>
                  <span class="end">09/10/2020 04:00 PM</span>
                  <span class="timezone">America/Los_Angeles</span>
                  <span class="title">Sygenta Virtual Field Days (Session 2)</span>
                  <span class="description">Interact live with Syngenta's breeding and sales teams, focussing on new products for USA, Canada and Mexico</span>
                  <span class="location">https://www.syngentavegetables-us.com/</span>
                </div>
                -->
                <div class="item-icon"><i class="fa fa-calendar-o" aria-hidden="true"></i></div>
                <div class="item-content">
                  <span class="item-title">Afternoon Session: 1 PM – 4 PM Pacific Time</span><br>
                  <em>Focus: USA, Canada and Mexico</em><br>
                </div>
              </div>
            </div>
          </div>
          <div class="row pt-4">
            <div class="col-12">
              <a href="#registration-form" class="btn btn-hollow">Register Now</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container pt-md-4 pb-5">
      <div class="row">
        <div class="col-12">
          <div class="banner-collage"></div>
        </div>
      </div>
    </div>

    <div class="container py-4 mb-5">
      <div class="row align-items-center">
        <div class="d-none d-xl-block col-xl-1"></div>
        <div class="col-12 col-md-6 col-lg-5 col-xl-4 pb-4 pb-md-0 px-5 px-lg-0">
          <img src="https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/08/10/logo.png" class="img-fluid mr-md-5 mb-md-5">
        </div>
        <div class="d-none d-lg-block col-lg-1"></div>
        <div class="col-12 col-md-6 text-center text-md-left">
          <h3 class="pb-3 d-none d-md-block">About Syngenta</h3>
          <p>
            We are a global leader at providing essential inputs to growers: crop protection, seeds, seed treatments and traits. We believe in delivering better food for a better world through outstanding crop solutions. We are devoted to helping growers do more with less. We take pride in meeting our commitments to our stakeholders. Our goal is to be the leading global provider of innovative products for every step of the agronomic process and to understand their interplay and optimize the results for growers and the food chain.
          </p>
        </div>
      </div>
    </div>
  </div>
`}
