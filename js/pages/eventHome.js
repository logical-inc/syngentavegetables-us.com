const PageHome = { template: `
  <div class="page-event-home page-home w-100">

    <div class="container-fluid banner banner-home">
      <div class="row">
        <div class="container">
          <div class="row justify-content-between position-relative">
            <div class="banner-content">
              <div class="banner-content-item">
                <h1 class="pb-4">Syngenta Americas Tour - Live!</h1>
                <p class="mb-0">
                  No travel, no problem. To ensure growers, seed distributors and partners along the value chain are still equipped with the latest products and innovations despite pandemic-related restrictions, Syngenta Vegetable Seeds is creating its first live, virtual field day event.
                </p>
                <div id="countbox" class="countbox mt-3 text-center"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container py-4">
      <div class="row py-4 justify-content-center align-items-stretch">
        <div class="col-12 d-flex flex-column align-items-center">
          <h4 class="pb-4 text-center" style="max-width: 875px;">
            Syngenta brings field days to screens across the Americas with live, virtual cucurbits event.
          </h4>
          <p class="text-center" style="max-width: 875px;">
            Thank you for joining us for Syngenta Americas Tour – Live! A Virtual Cucurbits Experience. We hope you enjoyed learning about the latest innovations in our cucurbit portfolio and connecting with international experts from the Syngenta Vegetable Seeds team.
            Hosted on September 10, 2020, you can rewatch the event by clicking on the video player below and browsing upcoming products.
          </p>
        </div>
        <div class="col-12 pt-5">
          <div class="embed-responsive embed-responsive-16by9" id="liveStream">
            <iframe src='https://vimeo.com/showcase/7559172/embed' id="liveStreamVideo" allow="autoplay; fullscreen" allowfullscreen frameborder='0'></iframe>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 id="browseProducts" class="text-uppercase text-center" style="line-height:1;">Upcoming Products</h2>
        </div>
        <div class="col-12 browse-products mt-4">
          <div class="row">

            <button class="browse-products-category watermelon collapsed" type="button" data-toggle="collapse" data-target="#categoryItemsWatermelon" aria-expanded="false" aria-controls="categoryItemsWatermelon">
              <div class="category-image"></div>
              <span class="category-title">Watermelon</span>
              <i class="fa fa-caret-down" aria-hidden="true"></i>
            </button>
            <div class="product-items collapse" id="categoryItemsWatermelon">
              <div class="row">

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Sirius" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/sirius">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/sirius-preview.jpg);"></div>
                    <div class="item-title">Sirius</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="WDL8416 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/wdl8416">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/wdl8416-preview.jpg);"></div>
                    <div class="item-title">WDL8416 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="WDL8415 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/wdl8415">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/wdl8415-preview.jpg);"></div>
                    <div class="item-title">WDL8415 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Powerhouse (WDL7401)" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/powerhouse-wdl7401">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/powerhouse-wdl7401-preview.jpg);"></div>
                    <div class="item-title">Powerhouse (WDL7401)</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Summerlicous (WDL6421)" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/summerlicous-wdl6421">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/summerlicous-wdl6421-preview.jpg);"></div>
                    <div class="item-title">Summerlicous (WDL6421)</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Endless Summer (WDL6429)" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/endless-summer-wdl6429">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/endless-summer-wdl6429-preview.jpg);"></div>
                    <div class="item-title">Endless Summer (WDL6429)</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Excursion" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/excursion">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/excursion-preview.jpg);"></div>
                    <div class="item-title">Excursion</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="WDL8418 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/wdl8418">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/wdl8418-preview.jpg);"></div>
                    <div class="item-title">WDL8418 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Deliciosa" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/deliciosa">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/deliciosa-preview.jpg);"></div>
                    <div class="item-title">Deliciosa</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Doxa ‡" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/doxa">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/doxa-preview.jpg);"></div>
                    <div class="item-title">Doxa &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="WMH3811 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/wmh3811">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/08/wmh3811-preview.jpg);"></div>
                    <div class="item-title">WMH3811 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Kahlo" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/kahlo">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/kahlo-preview.jpg);"></div>
                    <div class="item-title">Kahlo</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="WMH6875 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/wmh6875">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/wmh6875-preview.jpg);"></div>
                    <div class="item-title">WMH6875 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Liverpool" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/liverpool">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/liverpool-preview.jpg);"></div>
                    <div class="item-title">Liverpool</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Bahama" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/bahama">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/08/bahama-preview.jpg);"></div>
                    <div class="item-title">Bahama</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Sweet Gem" data-url="https://www.syngentavegetables-us.com/product/seed/watermelon/sweet-gem">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/08/sweet-gem-preview.jpg);"></div>
                    <div class="item-title">Sweet Gem</div>
                  </button>
                </div>

                <small class="pt-2 pb-3">&#8225;EXPERIMENTAL: Pre-Commercial Varieties are for reference purposes only. This material may not have been tested or substantiated in your market. <u>Not available to the public.</u></small>

                <a href="#categoryItemsWatermelon" class="close-category" type="button" data-toggle="collapse" data-target="#categoryItemsWatermelon" aria-expanded="false" aria-controls="categoryItemsWatermelon"><span>Close Watermelon</span><i class="fa fa-caret-up" aria-hidden="true"></i></a>

              </div>
            </div>

            <button class="browse-products-category melon collapsed" type="button" data-toggle="collapse" data-target="#categoryItemsMelon" aria-expanded="false" aria-controls="categoryItemsMelon">
              <div class="category-image"></div>
              <span class="category-title">Melon</span>
              <i class="fa fa-caret-down" aria-hidden="true"></i>
            </button>
            <div class="product-items collapse" id="categoryItemsMelon">
              <div class="row">

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Damaris (ME8892) ‡" data-url="https://www.syngentavegetables-us.com/product/seed/melon/damaris-me8892">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/08/damaris-me8892-preview.jpg);"></div>
                    <div class="item-title">Damaris (ME8892) &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Desert Express" data-url="https://www.syngentavegetables-us.com/product/seed/melon/desert-express">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/desert-express-preview.jpg);"></div>
                    <div class="item-title">Desert Express</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Garth" data-url="https://www.syngentavegetables-us.com/product/seed/melon/garth">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/garth-preview.jpg);"></div>
                    <div class="item-title">Garth</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Heidi (MS7350) ‡" data-url="https://www.syngentavegetables-us.com/product/seed/melon/heidi-ms7350">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/heidi-ms7350-preview.jpg);"></div>
                    <div class="item-title">Heidi (MS7350) &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Novira" data-url="https://www.syngentavegetables-us.com/product/seed/melon/novira">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/novira-preview.png);"></div>
                    <div class="item-title">Novira</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="MS8055 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/melon/ms8055">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/09/ms8055-preview.jpg);"></div>
                    <div class="item-title">MS8055 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Mamut" data-url="https://www.syngentavegetables-us.com/product/seed/melon/mamut">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/mamut-preview.jpg);"></div>
                    <div class="item-title">Mamut</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="TÂNTALO" data-url="https://www.syngentavegetables-us.com/product/seed/melon/tantalo">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/08/tantalo-preview.jpg);"></div>
                    <div class="item-title">TÂNTALO</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Salvatore" data-url="https://www.syngentavegetables-us.com/product/seed/melon/salvatore">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/salvatore-preview.jpg);"></div>
                    <div class="item-title">Salvatore</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Lume" data-url="https://www.syngentavegetables-us.com/product/seed/melon/lume">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/lume-preview.jpg);"></div>
                    <div class="item-title">Lume</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Titannium" data-url="https://www.syngentavegetables-us.com/product/seed/melon/titannium">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/titannium-preview.jpg);"></div>
                    <div class="item-title">Titannium</div>
                  </button>
                </div>

                <small class="pt-2 pb-3">&#8225;EXPERIMENTAL: Pre-Commercial Varieties are for reference purposes only. This material may not have been tested or substantiated in your market. <u>Not available to the public.</u></small>

                <a href="#categoryItemsMelon" class="close-category" type="button" data-toggle="collapse" data-target="#categoryItemsMelon" aria-expanded="false" aria-controls="categoryItemsWatermelon"><span>Close Melon</span><i class="fa fa-caret-up" aria-hidden="true"></i></a>

              </div>
            </div>

            <button class="browse-products-category squash collapsed" type="button" data-toggle="collapse" data-target="#categoryItemsSquash" aria-expanded="false" aria-controls="categoryItemsSquash">
              <div class="category-image"></div>
              <span class="category-title">Zucchini &amp; Squash</span>
              <i class="fa fa-caret-down" aria-hidden="true"></i>
            </button>
            <div class="product-items collapse" id="categoryItemsSquash">
              <div class="row">

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Payload" data-url="https://www.syngentavegetables-us.com/product/seed/squash/payload">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/payload-preview.jpg);"></div>
                    <div class="item-title">Payload</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="GN0070 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/squash/gn0070">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/08/gn0070-preview.jpg);"></div>
                    <div class="item-title">GN0070 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="GN0113 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/squash/gn0113">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/08/gn0113-preview.jpg);"></div>
                    <div class="item-title">GN0113 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Everglade" data-url="https://www.syngentavegetables-us.com/product/seed/squash/everglade">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/everglade-preview.jpg);"></div>
                    <div class="item-title">Everglade</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Spineless Supreme" data-url="https://www.syngentavegetables-us.com/product/seed/squash/spineless-supreme">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/spineless-beauty-preview.jpg);"></div>
                    <div class="item-title">Spineless Supreme</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="MG0477 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/squash/mg0477">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/09/mg0477-preview.jpg);"></div>
                    <div class="item-title">MG0477 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Ebano" data-url="https://www.syngentavegetables-us.com/product/seed/squash/ebano">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/ebano-preview.jpg);"></div>
                    <div class="item-title">Ebano</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="San Martin" data-url="https://www.syngentavegetables-us.com/product/seed/squash/san-martin">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/san-martin-gr0174-preview.jpg);"></div>
                    <div class="item-title">San Martin</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="GR0272 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/squash/gr0272">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/08/gr0272-preview.jpg  );"></div>
                    <div class="item-title">GR0272 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="GR0260 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/squash/gr0260">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/08/gr0260-preview.jpg);"></div>
                    <div class="item-title">GR0260 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Florinda" data-url="https://www.syngentavegetables-us.com/product/seed/squash/florinda">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/florinda-preview.jpg);"></div>
                    <div class="item-title">Florinda</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Brice" data-url="https://www.syngentavegetables-us.com/product/seed/squash/brice">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/brice-preview.jpg);"></div>
                    <div class="item-title">Brice</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="GZ0036 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/squash/gz0036">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/gz0036-preview.jpg);"></div>
                    <div class="item-title">GZ0036 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="DG0357 ‡" data-url="https://www.syngentavegetables-us.com/product/seed/squash/dg0357">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/dg357_d02-preview.jpg);"></div>
                    <div class="item-title">DG0357 &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Grandprize" data-url="https://www.syngentavegetables-us.com/product/seed/squash/grandprize">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/grandprize-preview.jpg);"></div>
                    <div class="item-title">Grandprize</div>
                  </button>
                </div>

                <small class="pt-2 pb-3">&#8225;EXPERIMENTAL: Pre-Commercial Varieties are for reference purposes only. This material may not have been tested or substantiated in your market. <u>Not available to the public.</u></small>

                <a href="#categoryItemsSquash" class="close-category" type="button" data-toggle="collapse" data-target="#categoryItemsSquash" aria-expanded="false" aria-controls="categoryItemsSquash"><span>Close Squash</span><i class="fa fa-caret-up" aria-hidden="true"></i></a>

              </div>
            </div>

            <button class="browse-products-category cucumber collapsed" type="button" data-toggle="collapse" data-target="#categoryItemsCucumber" aria-expanded="false" aria-controls="categoryItemsCucumber">
              <div class="category-image cucumber"></div>
              <span class="category-title">Cucumber</span>
              <i class="fa fa-caret-down" aria-hidden="true"></i>
            </button>
            <div class="product-items collapse" id="categoryItemsCucumber">
              <div class="row">

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Vidar (LC172620) ‡" data-url="https://www.syngentavegetables-us.com/product/seed/cucumber/vidar-lc172620">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/product-placeholder.jpg);"></div>
                    <div class="item-title">Vidar (LC172620) &#8225;</div>
                  </button>
                </div>

                <div class="product-item">
                  <button type="button" class="btn product-item-content" data-toggle="modal" data-target="#productDetailModal" data-title="Esben (LC172671) ‡" data-url="https://www.syngentavegetables-us.com/product/seed/cucumber/esben-lc172671">
                    <div class="item-image" style="background: url(https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/09/03/product-placeholder.jpg);"></div>
                    <div class="item-title">Esben (LC172671) &#8225;</div>
                  </button>
                </div>

                <small class="pt-2 pb-3">&#8225;EXPERIMENTAL: Pre-Commercial Varieties are for reference purposes only. This material may not have been tested or substantiated in your market. <u>Not available to the public.</u></small>

                <a href="#categoryItemsCucumber" class="close-category" type="button" data-toggle="collapse" data-target="#categoryItemsCucumber" aria-expanded="false" aria-controls="categoryItemsCucumber"><span>Close Cucumber</span><i class="fa fa-caret-up" aria-hidden="true"></i></a>

              </div>
            </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-12 d-flex flex-column flex-wrap align-items-center justify-content-center py-4 mt-4 text-center">
          <button type="button" class="btn btn-brand text-center d-inline-flex align-items-center justify-content-center" style="white-space: normal;" data-toggle="modal" data-target="#stayUpdatedModal" data-title="Stay Up to Date on New Products">
            <i class="fa fa-newspaper-o mt-1 mr-2" aria-hidden="true"></i>Stay up to date on New Products
          </button>
          <small class="pt-2">( or <a href="https://www.syngenta-us.com/crops/vegetabledetails" target="_blank"><u>Browse Syngenta's Full Commercial Portfolio</u></a>)</small>
        </div>
      </div>
    </div>

<!--
    <div class="container">
      <div class="row py-4 justify-content-center align-items-stretch">
        <div class="col-12 col-md-5 col-lg-5 col-xl-5 mb-5 mb-md-0 d-flex align-items-stretch">
          <div class="event-switch-image"></div>
        </div>
        <div class="col-12 pb-3 pb-md-0 col-md-7 col-lg-7 col-xl-7">
          <p>
            At Syngenta Vegetable Seeds, growers are at the heart of everything we do. We love being in
            the field with you, to share, to learn and to serve. 2020 has bought some new challenges to the
            seed industry, and we put safety first for our growers, partners and all individuals who work in
            our industry across the value chain. So, this year, stay safe and stay home and let Syngenta
            come to you virtually. To share our new products and innovations, please join us September 10,
            2020 for <b>Syngenta Americas Tour – Live! A Virtual Cucurbits Experience</b>.
          </p>
          <p>
            The live, virtual
            event will highlight the latest commercial and experimental cucurbit innovations in the Syngenta
            portfolio. During the event, which will be remotely broadcast from our research station in
            Woodland, attendees will have the opportunity to tune in for multiple sessions, then interact live
            with our breeding and sales team. Each session of the event will have its own regional focus,
            covering topics and varieties of key interest for those demographics, as well as speakers in
            multiple languages.
          </p>
        </div>
      </div>
    </div>
  -->

    <div class="container pt-md-4 pb-5">
      <div class="row">
        <div class="col-12">
          <div class="banner-collage"></div>
        </div>
      </div>
    </div>

    <div class="container py-4 mb-5 px-lg-4 px-xl-5">
      <div class="row align-items-center">
        <div class="d-none d-xl-block col-xl-1"></div>
        <div class="col-12 col-md-6 col-lg-5 col-xl-4 pb-4 pb-md-0 px-5 px-lg-0">
          <img src="https://www.syngentavegetables-us.com/sites/g/files/zhg1526/f/2020/08/10/logo.png" class="img-fluid mr-md-5 mb-md-5">
        </div>
        <div class="d-none d-lg-block col-lg-1"></div>
        <div class="col-12 col-md-6 text-center text-md-left">
          <h3 class="pb-3 d-none d-md-block">About Syngenta</h3>
          <p>
            We are a global leader at providing essential inputs to growers: crop protection, seeds, seed treatments and traits. We believe in delivering better food for a better world through outstanding crop solutions. We are devoted to helping growers do more with less. We take pride in meeting our commitments to our stakeholders. Our goal is to be the leading global provider of innovative products for every step of the agronomic process and to understand their interplay and optimize the results for growers and the food chain.
          </p>
        </div>
      </div>
    </div>

  </div>

  <!-- Product Detail Modal -->
  <div class="modal fade product-detail-modal" id="productDetailModal" tabindex="-1" role="dialog" aria-labelledby="ProductDetailLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="productDetailModalLabel">New message</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="button-text">Close</span>
            <span aria-hidden="true"><i class="fa fa-times-circle" aria-hidden="true"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="embed-responsive">
            <iframe src="" style="border:none;" id="productDetailIframe"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Stay Updated Signup Modal -->
  <div class="modal fade stay-updated-modal" id="stayUpdatedModal" tabindex="-1" role="dialog" aria-labelledby="StayUpdatedLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="stayUpdatedModalLabel">Stay Updated On Syngenta's Emerging Products</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="button-text">Close</span>
            <span aria-hidden="true"><i class="fa fa-times-circle" aria-hidden="true"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="embed-responsive">
            <iframe src="https://app.smartsheet.com/b/form/f0a955aa18fc43a49c39e9ff186a9040"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Ask a Question Modal -->
  <div class="modal fade ask-question-modal" id="askQuestionModal" tabindex="-1" role="dialog" aria-labelledby="askQuestionLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="askQuestionModalLabel">Ask a Question!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="button-text">Close</span>
            <span aria-hidden="true"><i class="fa fa-times-circle" aria-hidden="true"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <div class="embed-responsive">
            <iframe src="https://pigeonhole.at/SYNGENTAAMERICAS/i/1103127" style="border:none;"></iframe>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

`}
