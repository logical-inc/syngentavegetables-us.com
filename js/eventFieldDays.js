jQuery(document).ready(function () {

  //USED BY OTHER FUNCTIONS
  //Screen Size Stuff
  var $windowWidth;
  var $windowHeight;
  var $headerHeight;
  var $liveStreamHeight;

  function setHeights() {
    $windowWidth = jQuery(window).width();
    $windowHeight = jQuery(window).height();
    $headerHeight = jQuery(".main__header").outerHeight();
    $liveStreamHeight = jQuery("#liveStream").outerHeight();
    if ($headerHeight === undefined) {
      $headerHeight = 0;
    }
  }
  setHeights();


  //Smooth scroll to on-page targets
  // Select all links with hashes
  jQuery('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '')
        &&
        location.hostname === this.hostname
        ) {
        // Figure out element to scroll to
        var target = jQuery(this.hash);
        target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
        // Does a scroll target exist?
        if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        jQuery('html, body').animate({
          scrollTop: target.offset().top - (150) //ets the scroll to alightly above the target
          }, 1000, function () {
          //Setting focus causes a jump from the offset target. We simply unset the focus instead
          document.activeElement.blur();
        });
      }
    }
  }); //end smooth scroll



  //Browse Products operations

  jQuery('#productDetailModal').on('show.bs.modal', function (event) {
    var button = jQuery(event.relatedTarget); // Button that triggered the modal
    var productURL = button.data('url'); // Extract info from data-* attributes
    var productTitle = button.data('title');
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = jQuery(this)
    modal.find('.modal-title').text('New Product: ' + productTitle)
    modal.find('.modal-body input').val(productURL)
    jQuery(productDetailIframe).attr('src', productURL);
  })


  //Browse Products operations


  //Resize screen events
  jQuery(window).resize(function () {

      setHeights();

  });


  var vimeoIframe = jQuery("#liveStreamVideo");

  // $f == Froogaloop
  var player = $f(vimeoIframe);

  jQuery(".start-stream").click(function() {
    player.api("play");
  });

}); //Closes document.ready()
