Login to Syngenta Drupal platform here:

https://vegtrials.syngentacp.acsitefactory.com/user < master control
https://www.syngentavegetables-us.com/user < front-end items

The site was installed at the vegtrials URL. the sysgentavegetables-us.com version is accessed through the domain name that was eventually applied. https://www.syngentavegetables-us.com/ only works for getting front end URLs and similar items, and will fail if you attempt to update the CSS, HTML, or JavaScript tokens.

Reminders:
1. HTML, CSS, and JavaScript are controlled through Drupal tokens. Structure > Entity Types > Token
2. The header and closing tags must be deleted from index.html before it is pasted into the HTML token.
3. The Javascript token is only for header Javascript, which is mostly the Vue.js library.
4. The compiled footer JS must be pasted into Babel at https://babeljs.io/, and then pasted into the <script> tags in the footer. This enables Vue.js to work in IE11
5. eventFieldDays.js must then be pasted into the <script> tags after step 4.
6. It is now safe to update the HTML token.
7. Update CSS by pasting styles.min.css into the CSS token.
8. You probably won't have to update the JS token.
9. Unpublish and republish the page using the updated tokens
10. Clear cache. Content > Index (tab near top), scroll down to System > Performance.
